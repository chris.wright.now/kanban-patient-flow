# CS6440 Spring 2022
## Kanban Patient Flow

This is a partially implemented Kanban Patient Flow idea. It is based on code from the blog article [Vue.js Kanban Board: The Development Process](https://auth0.com/blog/vuejs-kanban-board-the-development-process).

## What it Demonstrates

This client code is intended to demonstrate how an emergency department at a hospital might use this format to create a patient card, then move that patient card through the system. In manufacturing Kanban, the card process makes assets trackable so the operation won't lose focus. This supports quality control, timeley execution, and trackability - all of which are valuable in the care delivery process as well.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```
